package com.ptc.ccp.ms.gocd.demo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import com.sm.javax.utilx.TimeUtils;

public class GoCdCreateArtifact
{

	public static void main (String[] args)
	{
		try (FileOutputStream fout = new FileOutputStream ("Artifact.txt"); PrintWriter writer = new PrintWriter (fout)) {
			writer.println ("BEGIN");
			writer.println ("  Time: " + TimeUtils.toSimpleDayHourMinSecFormat (TimeUtils.getNow ()));
			writer.println ("END");
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}
